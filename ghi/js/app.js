function createCard(name, description, pictureUrl, starts, ends, location) {
    let startsFormat = new Date(starts);
    let endsFormat = new Date(ends);
    return`
    <div class="col">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${startsFormat.toLocaleDateString()} - ${endsFormat.toLocaleDateString()}</div>
        </div>
      </div>
    </div>
    `;
}


function alertDanger(errorCode) {
    return `
    <div class="alert alert-danger" role="alert">
    An Error ${errorCode} has occurred! :(
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            let alertError = document.querySelector('h2');
            alertError.innerHTML += alertDanger(response.status);
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(
                        name,
                        description,
                        pictureUrl,
                        starts,
                        ends,
                        location
                        );
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.error(e);
            let alertError = document.querySelector('h2');
            alertError.innerHTML += alertDanger(response.status);
    }
 });
