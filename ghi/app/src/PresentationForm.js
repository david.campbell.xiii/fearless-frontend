import React, { useEffect, useState } from 'react';

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [presenterName, setPresenter] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.presenter_name = presenterName;
    data.company_name = companyName;
    data.presenter_email = presenterEmail;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    const confId = data.conference

    console.log(confId);
    const presentationurl = `http://localhost:8000${confId}presentations/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(presentationurl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);

      setPresenter('');
      setCompanyName('');
      setPresenterEmail('');
      setTitle('');
      setSynopsis('');
      setConference('');
    }
  }

  const handlePresenterChange = (event) => {
    const value = event.target.value;
    setPresenter(value);
  }
  const handleCompanyChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }
  const handlePresenterEmailChange = (event) => {
    const value = event.target.value;
    setPresenterEmail(value);
  }
  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  }
  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }
  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">

            <div className="form-floating mb-3">
              <input onChange={handlePresenterChange} value={presenterName} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
              <label htmlFor="presenter_name">Presenter Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleCompanyChange} value={companyName} placeholder="company_name" required type="text" name="company_name" id="company_name" className="form-control" />
              <label htmlFor="company_name">Company Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handlePresenterEmailChange} value={presenterEmail} placeholder="presenter_email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
              <label htmlFor="presenter_email">Presenter Email</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleTitleChange} value={title} placeholder="title" required type="text" name="title" id="title" className="form-control" />
              <label htmlFor="title">Title</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleSynopsisChange} value={synopsis} placeholder="synopsis" required type="text" name="synopsis" id="synopsis" className="form-control" />
              <label htmlFor="synopsis">Synopsis</label>
            </div>

            <div className="mb-3">
              <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                  return (
                    <option key={conference.href} value={conference.href}>{conference.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default PresentationForm;
